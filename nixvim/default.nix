{ pkgs, nixvim, ...}:
{

  imports = [
    nixvim.homeManagerModules.nixvim
    ./debug.nix
    ./noice.nix
    ./rust.nix
    ./treesitter.nix
    ./typescript.nix
  ];

  programs.bash.bashrcExtra = ''
    EDITOR=nvim
  '';

  programs.nixvim = {
    enable    = true;
    viAlias   = true;
    vimAlias  = true;


    colorschemes.catppuccin = {
      enable 	              = true;


      settings = {
        transparent_background = true;
        flavour = "mocha";
        integrations = {
          barbar      = true;
          cmp         = true;
          gitsigns    = true;
          noice       = true;
          notify      = true;
          nvimtree    = true;
          treesitter  = true;
        };
      };
    };


    opts = {
      number          = true;
      relativenumber  = true;
      expandtab       = true;
      tabstop	        = 2;
      shiftwidth      = 2;
    };

    extraPlugins = with pkgs.vimPlugins; [
      vim-hcl
      vim-flog
      vim-helm
      vim-gitbranch
      vim-airline-themes
    ];

    extraConfigLua = ''
      -- APPEARANCE
      vim.api.nvim_set_hl(0, 'LineNr', { fg = "#35b6e6" })

      require('gitsigns').setup()
    '';


    plugins = {
      chadtree.enable           = true;
      dap.enable                = true;
      diffview.enable           = true;
      fugitive.enable           = true;
      gitsigns.enable           = true;
      indent-blankline.enable   = true;
      lualine.enable            = true;
      luasnip.enable            = true;
      notify.enable             = true;
      nvim-tree.enable          = true;
      rainbow-delimiters.enable = true;
      rust-tools.enable         = true;
      toggleterm.enable         = true;
      
      barbar = {
        enable = true;
        animation = true;
      };
      
      airline = {
        enable = false;
        
        settings = {
          powerline_fonts = true;
          theme = "base16_dracula";
        };
      };

      telescope = {
        enable = true;
        
        settings = {
          pickers.find_files = {
            hidden = true;
            theme = "dropdown";
          };

          defaults = { 
            file_ignore_patterns = [
              "node_modules"
              "dist"
              ".git/"
              ".cache/"
            ];
          };
        };
        extensions = {
          file-browser.enable = true;
          fzf-native.enable   = true;
        };
      };

      lsp = {
        enable            = true;

        servers = {
          bashls.enable     = true;
          nixd.enable       = true;
          html.enable       = true;
          dockerls.enable   = true;
          lua-ls.enable     = true;
          jsonls.enable     = true;

          yamlls = {
            enable          = true;
            settings = {
              yaml.schemas = { 
                kubernetes = "*.yaml";
                "http://json.schemastore.org/github-workflow" = ".github/workflows/*";
                "http://json.schemastore.org/github-action" = ".github/action.{yml,yaml}";
                "http://json.schemastore.org/ansible-stable-2.9" = "roles/tasks/*.{yml,yaml}";
                "http://json.schemastore.org/prettierrc" = ".prettierrc.{yml,yaml}";
                "http://json.schemastore.org/kustomization" = "kustomization.{yml,yaml}";
                "http://json.schemastore.org/ansible-playbook" = "*play*.{yml,yaml}";
                "http://json.schemastore.org/chart" = "Chart.{yml,yaml}";
                "https://json.schemastore.org/dependabot-v2" = ".github/dependabot.{yml,yaml}";
                "https://json.schemastore.org/gitlab-ci" = "*gitlab-ci*.{yml,yaml}";
                "https://raw.githubusercontent.com/OAI/OpenAPI-Specification/main/schemas/v3.1/schema.json" = "*api*.{yml,yaml}";
                "https://raw.githubusercontent.com/compose-spec/compose-spec/master/schema/compose-spec.json" = "*docker-compose*.{yml,yaml}";
                "https://raw.githubusercontent.com/argoproj/argo-workflows/master/api/jsonschema/schema.json" = "*flow*.{yml,yaml}";
              };
            };
          };
        };

      };

      cmp = {
        enable            = true;
        settings = {
          sources = [
            { name = "nvim_lsp"; }
            { name = "luasnip"; }
            { name = "path"; }
            { name = "buffer"; }
          ];

          mapping = {
            "<C-Space>" = "cmp.mapping.complete()";
            "<C-e>"     = "cmp.mapping.close()";
            "<CR>"      = "cmp.mapping.confirm({ select = true })";
            "<S-Tab>"   = "cmp.mapping(cmp.mapping.select_prev_item(), { 'i', 's' })";
            "<Tab>"     = "cmp.mapping(cmp.mapping.select_next_item(), { 'i', 's' })";
            "<C-k>"     = "cmp.mapping.open_docs()";
            "<C-a>"     = "cmp.mapping.scroll_docs(-4)";
            "<C-q>"     = "cmp.mapping.scroll_docs(4)";
          };

          window = {
            completion.border = [ "╭" "─" "╮" "│" "╯" "─" "╰" "│"];
            documentation.border = [ "╭" "─" "╮" "│" "╯" "─" "╰" "│"];
          };

          formatting = {
            fields = [ "menu" "abbr" "kind" ];
            format = ''
              function(entry, item)
                local menu_icon = {
                  nvim_lsp = 'λ',
                  luasnip = '⋗',
                  buffer = 'Ω',
                  path = '🖫',
                } 
                item.menu = menu_icon[entry.source.name]
                return item
              end
            '';
          };


          snippet.expand = ''
            function(args)
              require('luasnip').lsp_expand(args.body)
            end
          '';

        };

      };
    };

    keymaps = [
      # Nvim-tree
      { mode = "n"; action = "<cmd>CHADopen<cr>"; key = "<C-M-b>"; }
      
      # Telescope
      { mode = "n"; action = "<cmd>Telescope find_files<cr>"; key = "<C-M-p>"; }

      # Terminal
      { mode = "t"; action = "<C-\\><C-n>"; key = "<esc>"; }
      { mode = ["t" "n"]; action = "<cmd>ToggleTerm<cr>"; key = "<C-M-k>"; }

      # Barbar
      { mode = ["n" "i"]; action = "<cmd>BufferPrevious<cr>"; key = "<A-h>"; }
      { mode = ["n" "i"]; action = "<cmd>BufferNext<cr>"; key = "<A-l>"; }
      { mode = ["n" "i"]; action = "<cmd>BufferClose<cr>"; key = "<C-A-w>"; }

      # LSP
      { mode = "n"; action = "<cmd>lua vim.lsp.buf.definition()<cr>"; key = "<C-D>"; }
      { mode = "n"; action = "<cmd>lua vim.lsp.buf.rename()<cr>"; key="<F2>"; }
      { mode = "n"; action = "<cmd>lua vim.lsp.buf.hover()<cr>"; key="<leader>h"; }
      { mode = "n"; action = "<cmd>Telescope live_grep<cr>"; key="<leader>lg"; }
    ];

    autoCmd = [{
      event = ["BufEnter" "CursorHold" "CursorHoldI" "FocusGained" ];
      pattern = [ "*" ];
      command = "if mode() != 'c' | checktime | endif";
    }];
  };

}

