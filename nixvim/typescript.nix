{ pkgs, config, nixvim, lib, ...}:
let
  nvim-dap-vscode-js = pkgs.vimUtils.buildVimPlugin {
    name = "vim-dap-vscode-js";
    src = pkgs.fetchFromGitHub {
      owner = "mxsdev";
      repo = "nvim-dap-vscode-js";
      rev = "e7c05495934a658c8aa10afd995dacd796f76091";
      sha256 = "sha256-lZABpKpztX3NpuN4Y4+E8bvJZVV5ka7h8x9vL4r9Pjk=";
    };
  };
in
{
  programs.nixvim = {
    plugins.lsp.servers = {
      tsserver.enable   = true;
      eslint.enable     = true;
      volar.enable      = true;
    };

    plugins.packer = {
      enable = true;
      plugins = [
        {
          name = "microsoft/vscode-js-debug";
          opt = true;
          run = "npm install --legacy-peer-deps && npx gulp vsDebugServerBundle && mv dist out";
        }
      ];
    };

    extraPlugins = with pkgs.vimPlugins; [
      nvim-dap-vscode-js
    ];

    extraConfigLua = ''
      local dap_vscode_js = require("dap-vscode-js")
      local languages = { "typescript", "javascript", "vue" }

      dap_vscode_js.setup({
        adapters = { 'pwa-node', 'pwa-chrome', 'pwa-msedge', 'node-terminal', 'pwa-extensionHost' }
      })

      -- DEBUG CONFIG TYPESCRIPT
      for _, lang in ipairs(languages) do
        dap.configurations.typescript = {
          {
            type = "pwa-node",
            request = "launch",
            name = "Launch file",
            program = "dist/''${fileBasenameNoExtension}.js",
            cwd = "''${workspaceFolder}",
          },
          {
            type = "pwa-node",
            request = "attach",
            name = "Attach to process",
            processId = require'dap.utils'.pick_process,
            cwd = "''${workspaceFolder}",
          },
          {
            type = "pwa-node",
            request = "launch",
            name = "Run Application",
            program = "dist/index.js",
            cwd = "''${workspaceFolder}",
          },
          {
            type = "pwa-node",
            request = "launch",
            name = "Run npm test",
            program = "node_modules/mocha/bin/_mocha",
            cwd = "''${workspaceFolder}",
          }
        }
      end
      -- DEBUG CONFIG JAVASCRIPT
    '';

    autoCmd = [{
      event = [ "BufWritePre" ] ;
      pattern = [ "*.tsx" "*.ts" "*.jsx" "*.js" ];
      command = "silent! EslintFixAll";
    }];
  };
}
