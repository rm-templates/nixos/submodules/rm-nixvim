# lldb needs to be installed. At the time of writing lldb_18 was installed
{ ... }:
{
  programs.nixvim = {
    plugins = {
      rust-tools.enable = true;

      lsp = {
        servers = {
          rust-analyzer = {
            enable = true;
            installCargo = true;
            installRustc = true;
          };
        };

        onAttach = ''
          local rt = require("rust-tools")
          vim.keymap.set("n", "<leader>k", rt.hover_actions.hover_actions, { buffer = bufnr })
        '';

      };
    };
    extraConfigLua = ''
        local rt = require("rust-tools")
        -- DEBUG RUST
        dap.adapters.lldb = {
          type = 'executable',
          command = '/run/current-system/sw/bin/lldb-vscode',
          name = 'lldb'
        }

        dap.adapters.gdb = {
          type = "executable",
          command = "gdb",
          args = { "-i", "dap" }
        }

        -- DEBUG CONFIG RUST
        dap.configurations.rust = {
          {
            name = 'Launch Debug Target',
            type = 'lldb',
            request = 'launch',
            program = "target/debug/''${workspaceFolderBasename}",
            cwd = "''${workspaceFolder}",
            stopOnEntry = false,
            args = {},
          },
        }

        -- DEBUG SETUP RUST TOOLS
        rt.setup({
          server = {
            on_attach = function(_, bufnr)
              vim.keymap.set("n", "<leader>H", rt.hover_actions.hover_actions, { buffer = bufnr })
              vim.keymap.set("n", "<leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
            end,
          },
        })
    '';

    keymaps = [
      
    ];
  };
}

